<div class="content-wrapper" style="background: white;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>Article List</small>
      </h1>
    </section>
    

    <section class="content">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td width="5%">
                No.
              </td>
              <td>
                Title
              </td>
              <td width="15%">
                Create time
              </td>
              <td width="15%">
                Publish time
              </td>
              <td colspan="2" width="15%">
                Action
              </td>
            </tr>
          </thead>

          <tbody>
            <?php foreach($articles as $k => $v){?>
                <tr>
                  <td>
                    <?php echo $k+1;?> 
                  </td>
                  <td>
                    <?php echo $v['title'];?> 
                  </td>
                  <td>
                    <?php echo $v['created'];?> 
                  </td>
                  <td>
                    <?php echo $v['published'];?> 
                  </td>
                  <td>
                    <a href="<?php echo base_url('edit/'.$v['id']); ?>" class="btn btn-primary">Edit</a>
                  </td>
                  <td>
                    <a href="<?php echo base_url('delete/'.$v['id']); ?>" class="btn btn-danger">Delete</a>
                  </td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
    </section>

    <section>
      <div class="col-xs-1">
        <?php if($articles[0]['id']){ ?>
          <a href="" class="btn btn-default">
            Back
          </a>
        <?php } ?>
      </div>
      <div class="col-xs-1 col-xs-offset-10">
        <a href="<?php echo base_url('dashboard/'.$articles[0]['id']); ?>" class="btn btn-default">
          Next
        </a>
      </div>
    </section>
</div>