<?php include('includes/header_landing.php'); ?>

<div class="header">
    <div class="container">
        <div class="display">
            <h1 class="display__tittle">Search Result</h1>
            <div class="display__btn-container input">
                <div class="input__wrap">
                    <input class="input__wrap__fill" type="search" placeholder="Search topic..." value="<?php echo $this->uri->segment(2);  ?>">
                    <img src="<?php echo base_url('assets/frontend') ?>/assets/search-icon-listnews.svg" onclick="searchBtn('<?php echo base_url(); ?>')">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="trending">
    <div class="container">
        <div class="trending__container">
            <?php if(!$articles){ ?>
                <div class="trending__container__no-data">
                    No Data
                </div>
            <?php } ?>
            <div class="trending__container__card card">
                <div class="card__wrapp no-grid">
                    <?php
                        foreach($articles as $k => $v){
                            ?>
                        <div class="card__wrapp__container">
                            <div class="card__wrapp__container-list">
                                <img class="card__wrapp__container-list-image" src="<?php echo base_url('assets/frontend') ?>/assets/notice-1.png" alt="">
                                <div class="card__wrapp__container-list__tittle">
                                    <div class="card__wrapp__container-list__tittle__date"><?php echo $v['publish'];?></div>
                                    <div class="card__wrapp__container-list__tittle__content"><?php echo $v['title'];?></div>
                                    <div class="card__wrapp__container-list__tittle__date"><?php echo $v['description'];?></div>
                                    <div class="card__wrapp__container-list__tittle__readmore">
                                        <a href="<?php echo base_url('detail/'.$v['id'])?>">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                            }
                        ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('includes/footer_landing.php'); ?>