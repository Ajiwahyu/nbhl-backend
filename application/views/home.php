<?php include('includes/header_landing.php'); ?>


<div class="header">
    <div class="container">
        <div class="display">
            <h1 class="display__tittle">Faster, Cheaper, and <br> more powerful DeFi</h1>
            <p class="display__paragraf">Easy M&A Investment Governance NBLS</p>
            <div class="display__btn-container">
                <button> <span>Whitepapper</span><img
                        src="<?php echo base_url('assets/frontend') ?>/assets/Korea-nbls.svg" alt=""></button>
                <button> <span>Whitepapper</span><img src="<?php echo base_url('assets/frontend') ?>/assets/UK-nbls.svg"
                        alt=""></button>
            </div>
            <div class="display__powered">
                <div class="display__powered-tittle">Powered :</div><img class="display__powered-img"
                    src="<?php echo base_url('assets/frontend') ?>/assets/powered.svg" alt="">
            </div>
        </div>
    </div>
</div>
<div class="platform">
    <div class="container">
        <div class="bg-video">
            <lottie-player src="<?php echo base_url('assets/frontend') ?>/assets/NBLS-lottie.json"
                background="transparent" speed="1" loop autoplay></lottie-player>
        </div>
        <div class="platform__tittle">Platform</div>
        <div class="platform__card">
            <div class="platform__card-items"><img class="platform__card-items-icon"
                    src="<?php echo base_url('assets/frontend') ?>/assets/icon-p-1.svg" alt="">
                <div class="platform__card-items-tittle">M&A Platform</div>
                <div class="platform__card-items-content">It utilizes blockchain technology to streamline the M&A
                    process and provide customers with high-cost and time-consuming M&A services. All transaction
                    elements related to the transaction are summarized and stored in the data store, and the stored
                    information is used to conduct M&A transactions. By using these functions, data logging and
                    access to transactions are quick and easy, saving customers time and money.</div>
            </div>
            <div class="platform__card-items"><img class="platform__card-items-icon"
                    src="<?php echo base_url('assets/frontend') ?>/assets/icon-p-2.svg" alt="">
                <div class="platform__card-items-tittle">Asset Management Platform</div>
                <div class="platform__card-items-content">The platform is operated by a team of experts who perform
                    functions separate from the investment banking experts who advise and operate the M&A platform.
                    Wealth managers perform the necessary investment research and due diligence to select the
                    appropriate financial assets to invest in. Wealth managers use the funds obtained from token
                    sales to finance off-chain and real-world investments, ICOs, or purchase promising
                    cryptocurrencies listed on exchanges.</div>
            </div>
            <div class="platform__card-items"><img class="platform__card-items-icon"
                    src="<?php echo base_url('assets/frontend') ?>/assets/icon-p-3.svg" alt="">
                <div class="platform__card-items-tittle">Exchange platform</div>
                <div class="platform__card-items-content">We will provide quality exchange trading services to
                    world-class investors and institutions. We will work closely with regulators and comply with all
                    KYC/AML standards required in the regions where our exchanges operate.</div>
            </div>
        </div>
    </div>
</div>
<div class="notice">
    <div class="container">
        <div class="notice__tittle">Notice
            <div class="notice__tittle__btn"> 
                <span>
                    <a href="<?php echo base_url('list')?>">See all</a>
                </span>
            </div>
        </div>
        <div class="notice__container">
            <div class="notice__container__card card">
                <?php
                        foreach($articles as $k => $v){
                            ?>
                <a class="card-items" href="<?php echo base_url('detail/'.$v['id'])?>">
                    <img class="card-items-image" src="<?php echo base_url('assets/frontend') ?>/assets/notice-1.png"
                        alt="">
                    <div class="card-items_media">
                        <div class="card-items_media-content"><?php echo $v['publish'];?></div>
                        <div class="card-items_media-tittle"><?php echo $v['title'];?></div>
                    </div>
                </a>
                <?php
                        }
                    ?>
            </div>
        </div>
    </div>
</div>
<div class="abstract">
    <div class="container">
        <div class="bg-video">
            <lottie-player src="<?php echo base_url('assets/frontend') ?>/assets/NBLS-lottie.json"
                background="transparent" speed="1" loop autoplay></lottie-player>
        </div>
        <div class="abstract__card">
            <div class="abstract__card-items"><img class="abstract__card-items-icon"
                    src="<?php echo base_url('assets/frontend') ?>/assets/1.svg" alt="">
                <div class="abstract__card-items-tittle">Abstract</div>
                <div class="abstract__card-items-content"> <span>NBLS, after its recent acquisition of a branch,
                        intends to become one of the leading blockchain-based management companies using encryption
                        technology and financial expertise to put customers at the forefront of future ventures. The
                        unprecedented acquisition of a key management branch marks the nation's first crypto-based
                        management development focused on growing businesses and strengthening corporate finances
                        through strategic investments.</span></div>
            </div>
            <div class="abstract__card-items flex-left"><img class="abstract__card-items-icon"
                    src="<?php echo base_url('assets/frontend') ?>/assets/2.svg" alt="">
                <div class="abstract__card-items-tittle">Solution</div>
                <div class="abstract__card-items-content"> <span>Collectively, this feature prioritizes the largely
                        anticipated components and benefits that traditional management systems lack, as well as the
                        additional dividends public investors will receive with NBLS Reflective Tokens. It will
                        maintain a coordinated and balanced ecosystem intertwined with the market.</span></div>
            </div>
        </div>
    </div>
</div>
<div class="unique">
    <div class="container">
        <div class="unique__tittle">NBLS Unique Selling Proposition</div>
        <div class="unique__content">
            <div class="unique__content__item">
                <div class="unique__content__item-tittle"><img
                        src="<?php echo base_url('assets/frontend') ?>/assets/1.svg" alt=""><span>Expanding Asset
                        Monetization</span></div>
                <div class="unique__content__item-subcontent"><span>Asset tokenization can be applied to both
                        traditional securities and new financial instruments and not only expands the asset types
                        under management, but also provides a financial system for NBLS customers to develop the
                        diversified ecosystem that underpins the cryptocurrency platform.</span></div>
            </div>
        </div>
        <div class="unique__content">
            <div class="unique__content__item">
                <div class="unique__content__item-tittle"><img
                        src="<?php echo base_url('assets/frontend') ?>/assets/2.svg" alt=""><span>Products that lag
                        behind market demand</span></div>
                <div class="unique__content__item-subcontent"><span>Assets can be customized to meet specific
                        investor needs, and slow-settling financial instruments can be reconfigured to better adapt
                        to changing demand. Diversity across the management sector has long been demanded as even
                        the most advisors and investors cannot predict market volatility and unforeseen factors.
                        However, through blockchain and digital asset integration, the new model provides an
                        improved framework for professionals to increase their profits.</span></div>
            </div>
        </div>
        <div class="unique__content">
            <div class="unique__content__item">
                <div class="unique__content__item-tittle"><img
                        src="<?php echo base_url('assets/frontend') ?>/assets/3.svg" alt=""><span>Broad market
                        access and low risk</span></div>
                <div class="unique__content__item-subcontent"><span>Fractionated ownership, reduced global
                        distribution and issuance costs make affordable assets accessible to large investors. Again,
                        risk reduction is paramount to every company and company on the planet, and NBLS with a
                        superior footing can be more profitable.</span></div>
            </div>
        </div>
        <div class="unique__content">
            <div class="unique__content__item">
                <div class="unique__content__item-tittle"><img
                        src="<?php echo base_url('assets/frontend') ?>/assets/4.svg" alt=""><span>Elevated secondary
                        market opportunities</span></div>
                <div class="unique__content__item-subcontent"><span>Increased connectivity between digital assets
                        and related networks expands secondary market opportunities and enhances liquidity
                        potential.</span></div>
            </div>
        </div>
        <div class="unique__content">
            <div class="unique__content__item">
                <div class="unique__content__item-tittle"><img
                        src="<?php echo base_url('assets/frontend') ?>/assets/5.svg" alt=""><span>Immediate
                        post-transaction processing and settlement</span></div>
                <div class="unique__content__item-subcontent"><span>Blockchain will enable instantaneous changes to
                        ownership records, transfers and settlements of funds, allowing lawyers and litigation to
                        take a back seat in disputes, while technology outlines facts as precisely and efficiently
                        as possible at a quarter of the cost. lead to the final benefit of NBLS</span></div>
            </div>
        </div>
        <div class="unique__content">
            <div class="unique__content__item">
                <div class="unique__content__item-tittle"><img
                        src="<?php echo base_url('assets/frontend') ?>/assets/6.svg" alt=""><span>Process efficiency
                        and cost savings</span></div>
                <div class="unique__content__item-subcontent"><span>At the heart of blockchain efficiency, smart
                        contracts automate asset practices including compliance, KYC/AML, registry maintenance,
                        payment settlement, and transaction monitoring and reporting. This saves considerable time
                        and money. <br> <br> Collectively, this feature prioritizes the largely anticipated
                        components and benefits that traditional management systems lack, as well as the additional
                        dividends public investors will receive with NBLS Reflective Tokens. It will maintain a
                        coordinated and balanced ecosystem intertwined with the general market.</span></div>
            </div>
        </div>
    </div>
</div>
<div class="roadmap">
    <div class="container">
        <h1 class="roadmap__tittle">Roadmap</h1>
        <div class="roadmap__content">
            <div class="roadmap__content__steps steps">
                <div class="steps__years">2021 Q2</div>
                <div class="steps__road">
                    <div class="steps__road__line border-line">
                        <div class="steps__road__line-point">
                            <div class="steps__road__line-point-inner"></div>
                        </div>
                    </div><img src="<?php echo base_url('assets/frontend') ?>/assets/dash-line.svg" alt="">
                </div>
                <div class="steps__list">
                    <ul>
                        <li> NBLS COIN launch</li>
                    </ul>
                </div>
            </div>
            <div class="roadmap__content__steps steps">
                <div class="steps__years">2022 Q1</div>
                <div class="steps__road">
                    <div class="steps__road__line">
                        <div class="steps__road__line-point">
                            <div class="steps__road__line-point-inner"></div>
                        </div>
                    </div><img src="<?php echo base_url('assets/frontend') ?>/assets/dash-line.svg" alt="">
                </div>
                <div class="steps__list">
                    <ul>
                        <li>NBLS website construction</li>
                        <li>NBLS coin stock acquisition in progress</li>
                        <li>Merger of NBLS House & NBLS Altar Establishment of European and Spanish national
                            headquarters</li>
                        <li>NBLS House direct management store Blings store ready</li>
                        <li>Blings name brand registration</li>
                        <li>NBLS People Asia Korea market 23 billion NBLS coins sales contract</li>
                    </ul>
                </div>
            </div>
            <div class="roadmap__content__steps steps">
                <div class="steps__years">2022 Q2</div>
                <div class="steps__road">
                    <div class="steps__road__line">
                        <div class="steps__road__line-point">
                            <div class="steps__road__line-point-inner"></div>
                        </div>
                    </div><img src="<?php echo base_url('assets/frontend') ?>/assets/dash-line.svg" alt="">
                </div>
                <div class="steps__list">
                    <ul>
                        <li>NBLS House direct management store Blancs store operation</li>
                        <li>NBLS House website launch & lodging business execution</li>
                        <li>NBLS Asia Korea office relocation</li>
                        <li>NBLS People Asia Exclusively sold 23 billion NBLS coins in Korea</li>
                        <li>NBLS website entry into English-speaking countries</li>
                        <li>Introduction of NBLS coin real payment service</li>
                        <li>Released e-wallet beta service</li>
                        <li>Listed on NBLS Coin Exchange</li>
                    </ul>
                </div>
            </div>
            <div class="roadmap__content__steps steps">
                <div class="steps__years">2022 Q3</div>
                <div class="steps__road">
                    <div class="steps__road__line">
                        <div class="steps__road__line-point">
                            <div class="steps__road__line-point-inner"></div>
                        </div>
                    </div><img src="<?php echo base_url('assets/frontend') ?>/assets/dash-line.svg" alt="">
                </div>
                <div class="steps__list">
                    <ul>
                        <li>1st listing on NBLS Coin (Overseas) Exchange (July)</li>
                        <li>2nd listing on NBLS Coin (Overseas) Exchange (August)</li>
                        <li>NBLS Coin (Overseas) CoinMarketCap registration</li>
                        <li>NBLS Staking Launch</li>
                        <li>Expansion of Blings stores in Spain (5 stores planned)</li>
                    </ul>
                </div>
            </div>
            <div class="roadmap__content__steps steps">
                <div class="steps__years">2022 Q4</div>
                <div class="steps__road">
                    <div class="steps__road__line border-line-r">
                        <div class="steps__road__line-point">
                            <div class="steps__road__line-point-inner"></div>
                        </div>
                    </div><img src="<?php echo base_url('assets/frontend') ?>/assets/dash-line.svg" alt="">
                </div>
                <div class="steps__list">
                    <ul>
                        <li>Preparing for the 3rd listing on the NBLS Coin (overseas) exchange</li>
                        <li>Preparing for listing on NBLS Coin (Korea) Exchange</li>
                        <li>Blings Coin Development & Release</li>
                        <li>Blings Overseas Coin Exchange Listing Registration</li>
                        <li>Expansion of Blings overseas stores (Portugal, Brazil)</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="tokenomic">
    <div class="container">
        <div class="tokenomic__tittle">Tokenomics</div>
        <div class="tokenomic__content">
            <div class="tokenomic__content-img"><img
                    src="<?php echo base_url('assets/frontend') ?>/assets/tokenomics-nbls.svg" alt=""></div>
            <div class="tokenomic__content-tabel">
                <table class="tokenomic__content-tabel--inner">
                    <tr>
                        <th>Token Allocation</th>
                        <th>Allocation</th>
                        <th>Vesting Schedule</th>
                    </tr>
                    <tr>
                        <td>Investment</td>
                        <td>15.00%</td>
                        <td>3 months cliff. vesting for 36 months linearly</td>
                    </tr>
                    <tr>
                        <td>Partners</td>
                        <td>2.00%</td>
                        <td>12 months cliff. vesting for 36 months linearly</td>
                    </tr>
                    <tr>
                        <td>Development</td>
                        <td>2.00%</td>
                        <td>Vesting for 60 months linearly</td>
                    </tr>
                    <tr>
                        <td>Team</td>
                        <td>10.00%</td>
                        <td>Vesting for 60 months linearly</td>
                    </tr>
                    <tr>
                        <td>Govermence Protocol</td>
                        <td>10.00%</td>
                        <td>Vesting for 60 months linearly</td>
                    </tr>
                    <tr>
                        <td>Liquidity and Mining Pool</td>
                        <td>20.00%</td>
                        <td>Vesting for 60 months linearly</td>
                    </tr>
                    <tr>
                        <td>Ecosystem</td>
                        <td>18.00%</td>
                        <td>Vesting for 60 months linearly</td>
                    </tr>
                    <tr>
                        <td>Governance Pool</td>
                        <td>1.00%</td>
                        <td>12 months cliff. vesting for 36 months linearly</td>
                    </tr>
                    <tr>
                        <td>Community Growth Funding</td>
                        <td>20.00%</td>
                        <td>12 months cliff. vesting for 36 months linearly</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="partner">
    <div class="container">
        <div class="partner__tittle">Partners</div>
        <div class="partner__sponsore"><img src="<?php echo base_url('assets/frontend') ?>/assets/partner-1.svg"
                alt=""><img src="<?php echo base_url('assets/frontend') ?>/assets/partner-2.svg" alt=""><img
                src="<?php echo base_url('assets/frontend') ?>/assets/partner-3.svg" alt=""><img
                src="<?php echo base_url('assets/frontend') ?>/assets/partner-4.svg" alt=""><img
                src="<?php echo base_url('assets/frontend') ?>/assets/partner-5.svg" alt=""></div>
    </div>
</div>


<?php include('includes/footer_landing.php'); ?>