<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Article_model extends CI_Model
{
    function getAll($limit = false, $trend = false, $lastId = 0)
    {
        $this->db->select('*, DATE_FORMAT(published, "%d %M %Y . %H:%m") as publish');
        $this->db->from('articles');
        $this->db->limit($limit ? $limit : 10);
        if($trend){
            $this->db->order_by('counter', 'DESC');
        }else{
            $this->db->order_by('published', 'DESC');
        }
        $this->db->where('deleted', NULL);
        $this->db->where('id >', $lastId);
        $query = $this->db->get();
        
        $articles = $query->result_array();
        
        if(!empty($articles)){
            return $articles;
        } else {
            return array();
        }
    }


    function getDetail($id){
        $this->db->select('*,DATE_FORMAT(published, "%d %M %Y . %H:%m") as publish');
        $this->db->from('articles');
        $this->db->join('categories' ,'articles.categoryId = categories.categoryId');
        $this->db->where('id', $id);
        $this->db->where('deleted', NULL);
        $query = $this->db->get();
        
        $article = $query->result();
        
        if(!empty($article)){
            return $article;
        } else {
            return array();
        }
    }

    function getSearch($query){
        $this->db->select('*,DATE_FORMAT(published, "%d %M %Y . %H:%m") as publish');
        $this->db->from('articles');
        $this->db->join('categories' ,'articles.categoryId = categories.categoryId');
        $this->db->where('deleted', NULL);
        $this->db->like('title', $query);
        $this->db->like('description', $query);
        $this->db->order_by('published', 'DESC');
        $query = $this->db->get();
        
        $articles = $query->result_array();
        
        if(!empty($articles)){
            return $articles;
        } else {
            return array();
        }
    }
    
}